#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup


setup(
    name='django-peace',
    version='1.0.0-dev',
    description='Lightweight REST microframework for Django.',
    author='Lucas Klassmann',
    author_email='lucasklassmann@gmail.com',
    url='https://github.com/klassmann/django-peace/',
    long_description=open('README.rst', 'r').read(),
    packages=[
        'peace',
    ],
    requires=[
        'django(>=1.7.1)',
    ],
    install_requires=[
        'django>=1.7.1',
    ],
    tests_require=[
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities'
    ],
)
