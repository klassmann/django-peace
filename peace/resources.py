# -*- coding: utf-8 -*-

import sys

from django.views.generic import View
from django.http import HttpResponse
from django.http import QueryDict
from django.views.decorators.csrf import csrf_exempt

from .exceptions import MethodNotImplemented
from .exceptions import EndpointNotFound
from .exceptions import BadRequest
from .exceptions import InvalidData
from .exceptions import Unauthorized

from .constants import OK
from .constants import CREATED
# from .constants import ACCEPTED
# from .constants import NO_CONTENT

from .serializers import JSONSerializer

from .utils import format_traceback


class Resource(View):
    debug = False
    endpoints_allowed = ['list', 'detail', ]
    request = None
    status_map = {
        'list': OK,
        'detail': OK,
        'create': CREATED,
        'update': OK,
        'delete': OK,
    }

    actions_map = {
        'list': {
            'GET': 'list',
            'POST': 'create'
        },
        'detail': {
            'GET': 'detail',
            'PUT': 'update',
            'DELETE': 'delete'
        }
    }

    serializer = JSONSerializer()

    def __serialize(self, data):
        return self.serializer.serialize(data)

    def __response(self, status, data=None):

        response = HttpResponse(content_type='application/json')

        if not data:
            data = {}

        serialized_data = self.__serialize(data)
        response.write(serialized_data)

        response.status_code = status
        return response

    def __exception_response(self, e):
        data = {}

        if isinstance(e, InvalidData):
            data = {
                'errors': self.__serialize(e.errors)
            }
        else:
            data = {
                'error': unicode(e),
            }

        if self.debug:
            data['traceback'] = format_traceback(sys.exc_info())

        status = getattr(e, 'status', 500)
        return self.__response(status, data)

    def __request_data(self, request):
        data = {}

        if request.method == 'GET':
            data = request.GET
        elif request.method == 'POST':
            data = request.POST
        elif request.method == 'PUT':
            data = QueryDict(request.body)

        return data

    @csrf_exempt
    def dispatch(self, request, endpoint, *args, **kwargs):
        try:
            self.request = request
            if not self.is_authenticated():
                raise Unauthorized()

            self.before_dispatch()

            method = request.method
            if endpoint in self.endpoints_allowed and endpoint in self.actions_map:
                actions = self.actions_map[endpoint]

                if method in actions:
                    action = actions[method]
                    handler = getattr(self, action, self.http_method_not_allowed)

                    request_data = self.__request_data(request)
                    response_data = None

                    if endpoint == 'detail':
                        if 'pk' in kwargs:
                            pk = int(kwargs['pk'])

                            if action == 'detail' or action == 'delete':
                                response_data = handler(pk)
                            else:
                                response_data = handler(pk, request_data)
                        else:
                            raise BadRequest()
                    else:
                        response_data = handler(request_data)

                    status = self.status_map[action]

                    return self.__response(status, response_data)
                else:
                    raise MethodNotImplemented()
            else:
                raise EndpointNotFound()

        except Exception as e:
            print 'Exception: ', unicode(e)
            return self.__exception_response(e)

        return super(Resource, self).dispatch(request, *args, **kwargs)

    def before_dispatch(self):
        pass

    def is_authenticated(self):
        return False

    def list(self, data):
        raise MethodNotImplemented()

    def detail(self, pk):
        raise MethodNotImplemented()

    def create(self, data):
        raise MethodNotImplemented()

    def update(self, pk, data):
        raise MethodNotImplemented()

    def delete(self, pk):
        raise MethodNotImplemented()
