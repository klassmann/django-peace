# -*- coding: utf-8 -*-

import datetime
import decimal

try:
    import json
except ImportError:
    import simplejson as json


class Serializer(object):
    def deserialize(self, body):
        raise NotImplementedError("Subclasses must implement this method.")

    def serialize(self, data):
        raise NotImplementedError("Subclasses must implement this method.")


class ExtendendJSONEncoder(json.JSONEncoder):
    """
    A JSON encoder that allows for more common Python data types.

    In addition to the defaults handled by ``json``, this also supports:

        * ``datetime.datetime``
        * ``datetime.date``
        * ``datetime.time``
        * ``decimal.Decimal``

    """
    def default(self, data):
        if isinstance(data, (datetime.datetime, datetime.date, datetime.time)):
            return data.isoformat()
        elif isinstance(data, decimal.Decimal):
            return str(data)
        else:
            return super(ExtendendJSONEncoder, self).default(data)

class JSONSerializer(Serializer):
    def deserialize(self, body):
        if isinstance(body, bytes):
            return json.loads(body.decode('utf-8'))
        return json.loads(body)

    def serialize(self, data):
        return json.dumps(data, cls=ExtendendJSONEncoder)
