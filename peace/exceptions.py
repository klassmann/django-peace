from .constants import APPLICATION_ERROR, UNAUTHORIZED, NOT_FOUND, BAD_REQUEST
from .constants import METHOD_NOT_ALLOWED, METHOD_NOT_IMPLEMENTED


class PeaceError(Exception):
    """
        Base class for peace exceptions.
    """
    pass


class HttpError(PeaceError):
    status_text = "Application Error"
    status_code = APPLICATION_ERROR

    def __init__(self, status_text=None):
        if not status_text:
            status_text = self.__class__.status_text

        super(HttpError, self).__init__(status_text)


class BadRequest(HttpError):
    status_text = "Bad request."
    status_code = BAD_REQUEST


class InvalidData(BadRequest):

    def __init__(self, errors={}):
        self.errors = errors


class Unauthorized(HttpError):
    status_code = UNAUTHORIZED
    status_text = "Unauthorized."


class NotFound(HttpError):
    status_code = NOT_FOUND
    status_text = "Resource not found."


class EndpointNotFound(NotFound):
    status_text = "Resource endpoint not found."


class MethodNotAllowed(HttpError):
    status_code = METHOD_NOT_ALLOWED
    status_text = "The specified HTTP method is not allowed."


class MethodNotImplemented(HttpError):
    status_code = METHOD_NOT_IMPLEMENTED
    status_text = "The specified HTTP method is not implemented."
