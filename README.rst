============
Django Peace
============

Lightweight REST microframework for Django_ (draft).

Inspired by restless_ and with focus on simplicity i wrote my own rest framework for some applications that i am developing. Great frameworks like `Django REST Framework`__ and Tastypie_ have a many features, but lost in the learning curve, and code is so extensive for comprehension.


This code is only a draft, i will make several changes that make more usable, and write a documentation soon.

You must read and use this code mainly for study or for make simple applications.

.. _Tastypie: http://tastypieapi.org/
.. _Django: http://djangoproject.com/
.. _Django_REST_Framework: http://www.django-rest-framework.org/
.. _restless: https://github.com/toastdriven/restless

__ Django_REST_Framework_

Licence
===========
MIT